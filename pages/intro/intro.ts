import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
    selector: 'page-intro',
    templateUrl: 'intro.html'
})

//To display intro/help page as soon as user opens the app
export class IntroPage {
constructor(public navCtrl: NavController) {}
    slides = [
    {
    title: "Nam porttitor bland.",
    description: "Nam porttitor blandit accumsan. Ut vel dictum sem, a preti.",
    image: "assets/imgs/intro1.png",
    },
    {
    title: "Fusce vehicula dolo",
    description: "Cras quis nulla commodo, aliquam lectus sed, blandit augue",
    image: "assets/imgs/intro2.png",
    },
    ];	
navHome() {
    this.navCtrl.setRoot(HomePage);
}
}
