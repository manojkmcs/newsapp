import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import 'rxjs/add/operator/map';

@Component({
selector: 'detailed-view',
templateUrl: 'detailed-view.html'
})
export class DetailedViewPage {

news: any;
constructor(public navCtrl: NavController, public navParams: NavParams) {
this.news = navParams.get('news');
}

dismiss() {
this.navCtrl.setRoot(HomePage);
}


}
