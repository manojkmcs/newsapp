import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';

@Component({
    selector: 'choose-newspaper',
    templateUrl: 'choose-newspaper.html'
})

//To display all the available news papers
export class ChooseNewspaperPage {

posts: any;
constructor(public navCtrl: NavController, public http: Http,private spinnerDialog: SpinnerDialog) {

    //http call to get list of all available news papers from newsapi.org
    this.spinnerDialog.show();
    this.http.get('https://newsapi.org/v2/sources?language=en&country=us&apiKey=17aebd882ccc42c6bfafd771b93b4f15').map(res => res.json()).subscribe(data => { 
    this.posts = data.sources;
    this.spinnerDialog.hide();
    });

}
dismiss() {
    this.navCtrl.setRoot(HomePage);
}


}
