import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailedViewPage } from '../detailed-view/detailed-view';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

//Landing page to list all the news posts with image, title and author information
export class HomePage {

posts: any;
constructor(public navCtrl: NavController, public http: Http,private spinnerDialog: SpinnerDialog) {

    //http get call to request data from newsapi.org
    this.spinnerDialog.show();
    this.http.get('https://newsapi.org/v2/everything?domains=wsj.com&apiKey=17aebd882ccc42c6bfafd771b93b4f15').map(res => res.json()).subscribe(data => { 
    this.posts = data.articles;
    this.spinnerDialog.hide();
    });
   	
}

detailedView(news) {
    this.navCtrl.push(DetailedViewPage, {
    news: news
});
}

}
